function createList(array) {
    const list = document.createElement('ul');
    document.querySelector('script').before(list);
    let newArray = array.map(function(item){
        let currentLi = document.createElement('li');
        currentLi.textContent = item;
        list.append(currentLi);
        return currentLi;
    });
    return newArray
};

 let result = createList(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv', 23]);
