const block = document.createElement('div');
// block.style.display = "inline-block";
const enterPrise = document.createElement('input');//спросить нужно ли поле для ввода только чисел
enterPrise.placeholder = "Price";
enterPrise.style.marginTop = "10px";


enterPrise.addEventListener('focus', function(){
    enterPrise.style.border = "1px solid green";
    enterPrise.style.outlineColor = "green";
});

enterPrise.addEventListener('blur', function (){
    if((enterPrise.value < 0) ) {
        enterPrise.style.border = "1px solid red";
        const errorPrice = document.createElement('span');
        errorPrice.innerHTML = "Please enter correct price";
        errorPrice.style.display = "block";
        document.querySelector('script').before(errorPrice);
        enterPrise.addEventListener('change', function(){
            if(enterPrise.value >= 0 || enterPrise.value === "") {
                errorPrice.remove();
            }
        });
    }else{
        enterPrise.style.border = "1px solid gray";

    }
});

enterPrise.addEventListener('change', function(){

    if(enterPrise.value >= 0){
        const inputValue = document.createElement('span');
        inputValue.style.display = "inline-block";
        inputValue.innerHTML = `Текущая цена: ${enterPrise.value}`;
        enterPrise.style.color = "green";
        document.querySelector('script').before(inputValue);
        block.appendChild(inputValue);

        const closeBlock = document.createElement('span');
        closeBlock.classList.add('close-block');
        document.querySelector('script').before(closeBlock);
        block.appendChild(closeBlock);



        closeBlock.addEventListener('click', function(){
               closeBlock.remove();
               enterPrise.value = "";
               inputValue.remove();
        });

        enterPrise.addEventListener('change', function(){
                inputValue.remove();
                closeBlock.remove();

        });
    }else{
        const closeBlock = document.createElement('span');
        closeBlock.classList.add('close-block');
        document.querySelector('script').before(closeBlock);
        block.appendChild(closeBlock);

        closeBlock.addEventListener('click', function(){
            closeBlock.remove();
            enterPrise.value = "";
        });
        enterPrise.addEventListener('change', function(){
            closeBlock.remove();

        });


    }


});





document.querySelector('script').before(block);
document.querySelector('script').before(enterPrise);
