function createNewUser(){
    const firstName = prompt("Enter your first name: ", 'Alyona');
    const lastName = prompt("Enter your last name: ", 'Kizko');
    const dateString = prompt("Enter date of your birthday:", '10.11.1999');
    let parseDate = dateString.split(".");
    let birthDate = new Date(`${parseDate[1]}/${parseDate[0]}/${parseDate[2]}`);

    const newUser = {
        firstName: firstName,
        lastName: lastName,
        birthday:  birthDate,
        getLogin(){
            return ((firstName[0] + lastName).toLowerCase());
        },
        getPassword(){
            return (firstName[0].toUpperCase() + lastName.toLowerCase() + birthDate.getFullYear())
        },
        getAge() {
        let today = new Date();
        let age = today.getFullYear() - birthDate.getFullYear();
        let m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age = age - 1;
        }
        return age;
        }
        };
    return newUser;
}

const newUser = createNewUser();
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());

