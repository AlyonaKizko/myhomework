
document.addEventListener('keydown', function(event){

    let btn = document.querySelectorAll('button');
    btn.forEach(function(elem){
        elem.classList.remove('active');
    });

    let arr = [];
    for(let i = 0; i < 7; i++){
        arr[i] = btn[i];
    }

    const btnText = arr.map(function(elem){
         return elem.textContent;
    });

    btnText.forEach(function(elem, index){
        const letterIn = event.code;

        if(letterIn === `Key${elem}` || letterIn === elem ){
            btn[index].classList.add('active');
        }
    });
});